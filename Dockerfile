FROM nginx:1.26.2

# Create app directory
RUN mkdir -p /var/www && mkdir -p /etc/nginx

# Bundle nginx configuration
COPY nginx /etc/nginx

# Certificate volume
VOLUME /etc/letsencrypt

# Expose port 80 and 443
EXPOSE 80 443
